FROM openjdk:12-alpine
EXPOSE 5000
ARG JAR_FILE=./build/libs/cars-api.jar
ADD ${JAR_FILE} app.jar
ENTRYPOINT ["java", "-jar", "/app.jar"]